/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc16.pkg4;

/**
 *
 * @author ionix
 */
public class Docente extends Empleados {
    private int nivel;
    private float horas, pagoHora;

    public Docente() {
        this.nivel = 0;
        this.horas = 0.0f;
        this.pagoHora = 0.0f;
    }

    public Docente(int nivel, float horas, float pagoHora, String nombre, String domicilio, Contrato contrato, int numEmp) {
        super(nombre, domicilio, contrato, numEmp);
        this.nivel = nivel;
        this.horas = horas;
        this.pagoHora = pagoHora;
    }

    public Docente(int nivel, float horas, float pagoHora) {
        this.nivel = nivel;
        this.horas = horas;
        this.pagoHora = pagoHora;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }
    
    @Override
    public float calcularTotal(){
        float Total=0.0f;
        switch (this.getNivel()) {
            case 1:
                Total = this.horas*this.pagoHora * 0.35f;
                break;
            case 2:
                Total = this.horas*this.pagoHora * 0.40f;
                break;
            case 3:
                Total = this.horas*this.pagoHora * 0.50f;
                break;
        }
        return Total;
    }
    
    @Override
    public float calcularImpuesto(){
        return this.horas*this.pagoHora*this.contrato.getImpuesto()/100;
    }
}
