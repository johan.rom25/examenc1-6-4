/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc16.pkg4;

import javax.swing.JOptionPane;

/**
 *
 * @author ionix
 */
public class jifPago extends javax.swing.JInternalFrame {

    /**
     * Creates new form jifPago
     */
    public jifPago() {
        initComponents();
        this.resize(881, 632);
        this.deshabilitar();
    }
    
    public void deshabilitar(){
        this.txtClave.setEnabled(false);
        this.txtDomicilio.setEnabled(false);
        this.txtHoraPago.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtNombre.setEnabled(false);
        this.txtNumEmpleado.setEnabled(false);
        this.txtPagoAdicional.setEnabled(false);
        this.txtPagoHora.setEnabled(false);
        this.txtPagoTotal.setEnabled(false);
        this.txtPagoImpuesto.setEnabled(false);
        this.txtPuesto.setEnabled(false);
        this.txtTotal.setEnabled(false);
        
        
        this.btnGuardar.setEnabled(false);
        this.btnMostrar.setEnabled(false);
    }
    
    public void habilitar(){
        this.txtClave.setEnabled(!false);
        this.txtDomicilio.setEnabled(!false);
        this.txtHoraPago.setEnabled(!false);
        this.txtNombre.setEnabled(!false);
        this.txtNumEmpleado.setEnabled(!false);
        this.txtPagoHora.setEnabled(!false);
        this.txtPuesto.setEnabled(!false);
        this.txtImpuesto.setEnabled(!false);
        
        this.btnGuardar.setEnabled(!false);
    }
    
    public void limpiar(){
        this.txtClave.setText("");
        this.txtDomicilio.setText("");
        this.txtHoraPago.setText("");
        this.txtImpuesto.setText("");
        this.txtNombre.setText("");
        this.txtNumEmpleado.setText("");
        this.txtPagoAdicional.setText("");
        this.txtPagoHora.setText("");
        this.txtPagoTotal.setText("");
        this.txtPagoImpuesto.setText("");
        this.txtPuesto.setText("");
        
        this.cmbNivel.setSelectedIndex(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtClave = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtNumEmpleado = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtDomicilio = new javax.swing.JTextField();
        txtPuesto = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtImpuesto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtPagoHora = new javax.swing.JTextField();
        cmbNivel = new javax.swing.JComboBox<>();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        jLabel8 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox<>();
        txtHoraPago = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtTotal = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtPagoAdicional = new javax.swing.JTextField();
        txtPagoImpuesto = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtPagoTotal = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        btnNuevo = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Pago");
        getContentPane().setLayout(null);

        txtClave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClaveActionPerformed(evt);
            }
        });
        getContentPane().add(txtClave);
        txtClave.setBounds(140, 20, 150, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Num. Empleado:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 60, 110, 30);

        txtNumEmpleado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtNumEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumEmpleadoActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumEmpleado);
        txtNumEmpleado.setBounds(140, 60, 150, 30);

        txtNombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        getContentPane().add(txtNombre);
        txtNombre.setBounds(140, 100, 260, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Nombre:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 100, 110, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Domicilio:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(30, 140, 110, 30);

        txtDomicilio.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtDomicilio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDomicilioActionPerformed(evt);
            }
        });
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(140, 140, 260, 30);

        txtPuesto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPuestoActionPerformed(evt);
            }
        });
        getContentPane().add(txtPuesto);
        txtPuesto.setBounds(140, 180, 150, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Puesto:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 180, 110, 30);

        txtImpuesto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });
        getContentPane().add(txtImpuesto);
        txtImpuesto.setBounds(140, 220, 150, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Nivel:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(30, 260, 110, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Pago x Hora:");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(30, 300, 110, 30);

        txtPagoHora.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPagoHora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoHoraActionPerformed(evt);
            }
        });
        getContentPane().add(txtPagoHora);
        txtPagoHora.setBounds(140, 300, 150, 30);

        cmbNivel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cmbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nivel Maestria", "Nivel 1", "Nivel 2", "Nivel 3" }));
        getContentPane().add(cmbNivel);
        cmbNivel.setBounds(140, 260, 150, 30);

        jInternalFrame1.setClosable(true);
        jInternalFrame1.setIconifiable(true);
        jInternalFrame1.setMaximizable(true);
        jInternalFrame1.setResizable(true);
        jInternalFrame1.setTitle("Pago");
        jInternalFrame1.getContentPane().setLayout(null);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Clave Contrato:");
        jInternalFrame1.getContentPane().add(jLabel8);
        jLabel8.setBounds(30, 20, 110, 30);

        jTextField8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });
        jInternalFrame1.getContentPane().add(jTextField8);
        jTextField8.setBounds(140, 20, 150, 30);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Num. Empleado:");
        jInternalFrame1.getContentPane().add(jLabel9);
        jLabel9.setBounds(30, 60, 110, 30);

        jTextField9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField9ActionPerformed(evt);
            }
        });
        jInternalFrame1.getContentPane().add(jTextField9);
        jTextField9.setBounds(140, 60, 150, 30);

        jTextField10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });
        jInternalFrame1.getContentPane().add(jTextField10);
        jTextField10.setBounds(140, 100, 260, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Nombre:");
        jInternalFrame1.getContentPane().add(jLabel10);
        jLabel10.setBounds(30, 100, 110, 30);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("Domicilio:");
        jInternalFrame1.getContentPane().add(jLabel11);
        jLabel11.setBounds(30, 140, 110, 30);

        jTextField11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField11ActionPerformed(evt);
            }
        });
        jInternalFrame1.getContentPane().add(jTextField11);
        jTextField11.setBounds(140, 140, 260, 30);

        jTextField12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField12ActionPerformed(evt);
            }
        });
        jInternalFrame1.getContentPane().add(jTextField12);
        jTextField12.setBounds(140, 180, 150, 30);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setText("Puesto:");
        jInternalFrame1.getContentPane().add(jLabel12);
        jLabel12.setBounds(30, 180, 110, 30);

        jTextField13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField13ActionPerformed(evt);
            }
        });
        jInternalFrame1.getContentPane().add(jTextField13);
        jTextField13.setBounds(140, 220, 150, 30);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setText("Impuesto:");
        jInternalFrame1.getContentPane().add(jLabel13);
        jLabel13.setBounds(30, 220, 110, 30);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setText("Impuesto:");
        jInternalFrame1.getContentPane().add(jLabel14);
        jLabel14.setBounds(30, 300, 110, 30);

        jTextField14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField14ActionPerformed(evt);
            }
        });
        jInternalFrame1.getContentPane().add(jTextField14);
        jTextField14.setBounds(140, 300, 150, 30);

        jComboBox2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nivel Maestria", "Nivel 1", "Nivel 2", "Nivel 3" }));
        jInternalFrame1.getContentPane().add(jComboBox2);
        jComboBox2.setBounds(140, 260, 150, 30);

        getContentPane().add(jInternalFrame1);
        jInternalFrame1.setBounds(0, 0, 120, 45);

        txtHoraPago.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtHoraPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHoraPagoActionPerformed(evt);
            }
        });
        getContentPane().add(txtHoraPago);
        txtHoraPago.setBounds(140, 340, 150, 30);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("Horas Trabajadas:");
        getContentPane().add(jLabel15);
        jLabel15.setBounds(30, 340, 110, 30);

        jPanel1.setBackground(new java.awt.Color(255, 0, 0));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 24))); // NOI18N
        jPanel1.setLayout(null);

        txtTotal.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtTotal);
        txtTotal.setBounds(160, 50, 150, 30);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Total:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(30, 50, 110, 30);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Pago Adicional:");
        jPanel1.add(jLabel17);
        jLabel17.setBounds(30, 100, 110, 30);

        txtPagoAdicional.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtPagoAdicional);
        txtPagoAdicional.setBounds(160, 100, 150, 30);

        txtPagoImpuesto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtPagoImpuesto);
        txtPagoImpuesto.setBounds(160, 150, 150, 30);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Pago Impuesto:");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(30, 150, 110, 30);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setText("Pago Total:");
        jPanel1.add(jLabel19);
        jLabel19.setBounds(30, 200, 110, 30);

        txtPagoTotal.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtPagoTotal);
        txtPagoTotal.setBounds(160, 200, 150, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(390, 240, 370, 290);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Clave Contrato:");
        getContentPane().add(jLabel16);
        jLabel16.setBounds(30, 20, 110, 30);

        btnNuevo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(470, 90, 73, 60);

        btnMostrar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(690, 90, 79, 60);

        btnGuardar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(580, 90, 81, 60);

        btnLimpiar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(20, 470, 75, 25);

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(130, 470, 85, 25);

        btnCerrar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(240, 470, 71, 25);

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setText("Impuesto:");
        getContentPane().add(jLabel20);
        jLabel20.setBounds(30, 220, 110, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClaveActionPerformed

    private void txtNumEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumEmpleadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumEmpleadoActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtDomicilioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDomicilioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDomicilioActionPerformed

    private void txtPuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPuestoActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void txtPagoHoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoHoraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoHoraActionPerformed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void jTextField9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField9ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void jTextField11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField11ActionPerformed

    private void jTextField12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField12ActionPerformed

    private void jTextField13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField13ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField13ActionPerformed

    private void jTextField14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField14ActionPerformed

    private void txtHoraPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHoraPagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHoraPagoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        boolean exito=false;
        if(this.txtClave.getText().equals("")){
            exito=true;
        }
        if(this.txtDomicilio.getText().equals("")){
            exito=true;
        }
        if(this.txtNombre.getText().equals("")){
            exito=true;
        }
        if(this.txtHoraPago.getText().equals("")){
            exito=true;
        }
        if(this.txtPagoHora.getText().equals("")){
            exito=true;
        }
        if(this.txtNumEmpleado.getText().equals("")){
            exito=true;
        }
        if(this.txtPuesto.getText().equals("")){
            exito=true;
        }
        if(this.txtImpuesto.getText().equals("")){
            exito=true;
        }
        if(this.cmbNivel.getSelectedIndex()==0){
            exito=true;
        }
        if (exito == true) {
            JOptionPane.showMessageDialog(this, "Falto capturar información");
        }
        else{
            try{
                doc.setContrato(con);
                doc.setDomicilio(this.txtDomicilio.getText());
                doc.setHoras(Float.parseFloat(this.txtHoraPago.getText()));
                doc.setNivel(this.cmbNivel.getSelectedIndex());
                doc.setNombre(this.txtNombre.getText());
                doc.setNumEmp(Integer.parseInt(this.txtNumEmpleado.getText()));
                doc.setPagoHora(Float.parseFloat(this.txtPagoHora.getText()));
                con.setClave(Integer.parseInt(this.txtClave.getText()));
                con.setImpuesto(Float.parseFloat(this.txtImpuesto.getText()));
                con.setPuesto(this.txtPuesto.getText());
            } catch (NumberFormatException e) {
                exito = true;
                JOptionPane.showMessageDialog(this, "Surgio un error " + e.getMessage());
                this.deshabilitar();
                this.limpiar();
        }
        if (exito == false) {
                this.btnMostrar.setEnabled(true);
                JOptionPane.showMessageDialog(this, "Se guardó la informacion con exito");
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        doc = new Docente();
        con = new Contrato();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        this.txtImpuesto.setText(String.valueOf(con.getImpuesto()));
        this.txtNumEmpleado.setText(String.valueOf(doc.getNumEmp()));
        this.txtPagoHora.setText(String.valueOf(doc.getPagoHora()));
        this.txtHoraPago.setText(String.valueOf(doc.getHoras()));
        this.txtTotal.setText(String.valueOf(doc.getHoras()*doc.getPagoHora()));
        this.txtPagoImpuesto.setText(String.valueOf(doc.calcularImpuesto()));
        this.txtPagoAdicional.setText(String.valueOf(doc.calcularTotal()));
        this.txtPagoTotal.setText(String.valueOf((doc.getHoras()*doc.getPagoHora())+doc.calcularTotal()-doc.calcularImpuesto()));
        this.txtClave.setText(String.valueOf(con.getClave()));
        this.txtDomicilio.setText(doc.getDomicilio());
        this.txtNombre.setText(doc.getNombre());
        this.txtPuesto.setText(con.getPuesto());
        
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
         int op = 0;

        op = JOptionPane.showConfirmDialog(this, "¿Realmente quieres cerrar?", "Empleados", JOptionPane.YES_NO_OPTION);

        if (op == JOptionPane.YES_OPTION) {

            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
         this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbNivel;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JTextField txtClave;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtHoraPago;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumEmpleado;
    private javax.swing.JTextField txtPagoAdicional;
    private javax.swing.JTextField txtPagoHora;
    private javax.swing.JTextField txtPagoImpuesto;
    private javax.swing.JTextField txtPagoTotal;
    private javax.swing.JTextField txtPuesto;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
    private Contrato con;
    private Docente doc;

}
